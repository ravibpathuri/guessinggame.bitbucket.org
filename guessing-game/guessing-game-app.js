var app= angular.module('root', []);

//app.value("game", new guessingGame());

app.controller("gameController", ["$scope", function ($scope) {
    var game;
    initGame();
    $scope.play = function () {
        
        game.play($scope.guess);
        $scope.result = game.result;

        $scope.messages.push({ message: game.result.message, guess: $scope.guess });

        console.log(game.result.message);
    };

    $scope.isBegining = function () { return game.result.attempts == 0; };

    $scope.isGameOver = function () { return game.result.isGameOver; };

    $scope.newGame = function () {
        if (game.result.isGameOver) initGame();
    };

    function initGame() {
        game = new guessingGame();
        $scope.result = game.result;
        $scope.messages = [];
    };
}]);