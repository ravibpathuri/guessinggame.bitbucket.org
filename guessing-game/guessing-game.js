var guessingGame = function () {

    var target = Math.floor(Math.random() * 100);

    var result = { isGameOver: false, message: null, attempts: 0 };

    function playGame(guess) {
        if (result.isGameOver) return;
        result.attempts++;
        if (guess < target)
            result.message = "Aim Higher";
        else if (guess > target)
            result.message = "Aim Lower";
        else {
            result.isGameOver = true;
            result.message = "You've got it!!!";
        }
    }

    return {
        play: playGame,
        result: result,
    }

};